from django.conf.urls import url, include
from rest_framework import routers
from core.views.v1_truck import TruckViewSet as V1TruckViewSet

__author__ = 'vatsalshah'

v1_router = routers.DefaultRouter()
v1_router.register(r'truck', V1TruckViewSet)

urlpatterns = [
    url(r'api/v1/', include(v1_router.urls)),
]
