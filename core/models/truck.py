from django.db import models


class Truck(models.Model):
    """
    Truck model replicating all the data from SF OpenData.
    """
    created_at = models.DateTimeField(
        verbose_name='created at',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='updated at',
        auto_now=True
    )
    SOURCE_TYPES = (
        ('SFOD', 'SF OpenData'),
        ('N', 'Not Applicable')
    )
    source = models.CharField(
        verbose_name='entry source',
        max_length=5,
        choices=SOURCE_TYPES,
        default='N'
    )
    objectid = models.IntegerField(
        verbose_name='locationid',
        unique=True
    )
    applicant = models.TextField(
        verbose_name='Applicant'
    )
    facilitytype = models.TextField(
        verbose_name='FacilityType'
    )
    cnn = models.IntegerField(
        verbose_name='cnn',
        blank=True,
        null=True
    )
    locationdescription = models.TextField(
        verbose_name='LocationDescription',
        blank=True,
        null=True
    )
    address = models.TextField(
        verbose_name='Address'
    )
    blocklot = models.TextField(
        verbose_name='blocklot',
        blank=True,
        null=True
    )
    block = models.TextField(
        verbose_name='block',
        blank=True,
        null=True
    )
    lot = models.TextField(
        verbose_name='lot',
        blank=True,
        null=True
    )
    permit = models.TextField(
        verbose_name='permit',
        blank=True,
        null=True
    )
    status = models.TextField(
        verbose_name='Status'
    )
    fooditems = models.TextField(
        verbose_name='FoodItems',
        blank=True,
        null=True
    )
    x = models.FloatField(
        verbose_name='X',
        blank=True,
        null=True
    )
    y = models.FloatField(
        verbose_name='Y',
        blank=True,
        null=True
    )
    latitude = models.FloatField(
        verbose_name='Latitude',
        blank=True,
        null=True
    )
    longitude = models.FloatField(
        verbose_name='Longitude',
        blank=True,
        null=True
    )
    schedule = models.TextField(
        verbose_name='Schedule',
        
        blank=True,
        null=True
    )
    dayshours = models.TextField(
        verbose_name='dayshours',
        blank=True,
        null=True
    )
    noisent = models.DateTimeField(
        verbose_name='NOISent',
        blank=True,
        null=True
    )
    approved = models.DateTimeField(
        verbose_name='Approved',
        blank=True,
        null=True
    )
    received = models.TextField(
        verbose_name='Received',
        blank=True,
        null=True
    )
    priorpermit = models.IntegerField(
        verbose_name='PriorPermit',
        blank=True,
        null=True
    )
    expirationdate = models.DateTimeField(
        verbose_name='ExpirationDate',
        blank=True,
        null=True
    )
    location_city = models.TextField(
        verbose_name='Location (city)',
        blank=True,
        null=True
    )
    location_address = models.TextField(
        verbose_name='Location (address)',
        blank=True,
        null=True
    )
    location_zip = models.TextField(
        verbose_name='Location (zip)',
        blank=True,
        null=True
    )
    location_state = models.TextField(
        verbose_name='Location (state)',
        blank=True,
        null=True
    )
