from rest_framework import serializers

from core.models.truck import Truck


class TruckSerializer(serializers.ModelSerializer):
    """
    Truck Model Serializer for DRF
    """

    class Meta:
        model = Truck
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'source')
