import json

import django_filters
from geopy.distance import vincenty
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions

from core.models.truck import Truck
from core.serializers.v1_truck import TruckSerializer


# Function to filter the queryset based on current user location
def nearest_trucks(queryset, value):
    if not value:
        return queryset
    value_obj = json.loads(value)
    filtered_trucks = []
    for truck in queryset:
        distance = vincenty((value_obj['latitude'], value_obj['longitude']), (truck.latitude, truck.longitude)).miles
        if distance < value_obj['distance']:
            filtered_trucks.append(truck.objectid)
    return queryset.filter(objectid__in=filtered_trucks)


class TrackViewSetFilter(django_filters.FilterSet):
    """
    Filters for the viewset
    """
    location = django_filters.CharFilter(action=nearest_trucks)
    food_items = django_filters.CharFilter(name='fooditems', lookup_type='icontains')

    class Meta:
        model = Truck
        fields = ['fooditems']


class TruckViewSet(viewsets.ModelViewSet):
    """
    ViewSet for viewing food trucks.
    """
    queryset = Truck.objects.all()
    serializer_class = TruckSerializer
    permission_classes = [DjangoModelPermissions]
    filter_class = TrackViewSetFilter
    http_method_names = ['get']
