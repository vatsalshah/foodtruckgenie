import json
import logging
import os
from concurrent import futures
from FoodTruckGenie.celery import app

import requests

logger = logging.getLogger('ftgenie-core')


@app.task
def store_truck_data(obj):
    from core.models.truck import Truck
    logger.info("Creating new truck for objectid: {}".format(obj['objectid']))
    new_truck = Truck(
        source='SFOD',
        objectid=int(obj['objectid']),
        applicant=obj['applicant'],
        facilitytype=obj['facilitytype'],
        cnn=int(obj['cnn']) if 'cnn' in obj else None,
        locationdescription=obj['locationdescription'] if 'locationdescription' in obj else None,
        address=obj['address'],
        blocklot=obj['blocklot'] if 'blocklot' in obj else None,
        block=obj['block'] if 'block' in obj else None,
        lot=obj['lot'] if 'lot' in obj else None,
        permit=obj['permit'] if 'permit' in obj else None,
        status=obj['status'],
        fooditems=obj['fooditems'] if 'fooditems' in obj else None,
        x=float(obj['x']) if 'x' in obj else None,
        y=float(obj['y']) if 'y' in obj else None,
        latitude=float(obj['latitude']) if 'latitude' in obj else None,
        longitude=float(obj['longitude']) if 'longitude' in obj else None,
        schedule=obj['schedule'] if 'schedule' in obj else None,
        dayshours=obj['dayshours'] if 'dayshours' in obj else None,
        noisent=obj['noisent'] if 'noisent' in obj else None,
        approved=obj['approved'] if 'approved' in obj else None,
        received=obj['received'] if 'received' in obj else None,
        priorpermit=int(obj['priorpermit']) if 'priorpermit' in obj else None,
        expirationdate=obj['expirationdate'] if 'expirationdate' in obj else None,
        location_city=obj['location_city'] if 'location_city' in obj else None,
        location_address=obj['location_address'] if 'location_address' in obj else None,
        location_zip=obj['location_zip'] if 'location_zip' in obj else None,
        location_state=obj['location_state'] if 'location_state' in obj else None
    )
    try:
        new_truck.save()
        logger.info("{} for objectid {} saved successfully".format(new_truck.pk, obj['objectid']))
    except Exception as e:
        logger.error("Error occured while saving objectid {}: {}".format(obj['objectid'], str(e)))


@app.task
def update_truck_model():
    auth_token = os.environ.get('SFOD_TOKEN', None)
    if auth_token is not None:
        headers = {'X-App-Token': auth_token}
    else:
        headers = {}
    params = {'status': 'APPROVED'}
    request = requests.get('http://data.sfgov.org/resource/rqzj-sfat.json', headers=headers, params=params)
    data = json.loads(request.text)
    logger.info("{} approved trucks found".format(len(data)))
    with futures.ThreadPoolExecutor(max_workers=20) as executor:
        futures_track = (executor.submit(store_truck_data.delay, item) for item in data)
        for result in futures.as_completed(futures_track):
            if result.exception() is not None:
                logger.error('%s' % result.exception())
            else:
                logger.info(result.result())
