# Coding Challenge

A rapidly built prototype (Codename: FoodTruckGenie) to exhibit a simple web application for finding food trucks based 
on location and other user preferences around the San Francisco area.

## Features

- Production-ready configuration for Static Files, Database Settings, Gunicorn, etc.
- Enhancements to Django's static file serving functionality via WhiteNoise
- Enhancements to Django's database functionality via django-postgrespool and dj-database-url
- Enhancements to Django's default structure for code readibility and scalability
- REST APIs w/ token authentication and advanced filtering for communication with the web app
- Celery to run scheduled and background tasks including email triggers
- Setup to traverse easily through the environment pipeline
- Advanced logging

## Demo Staging Site

- [Web App](https://foodtruckgenie.herokuapp.com)
- [API Docs](http://docs.foodtruckgenie.apiary.io/)

## How to Use

Pre-requisites
- Python 3.5.0
- Redis Server
- Postgresql
- Celery

To use this project, follow these steps:

1. Create your working environment.
2. Clone this project. (`$ git clone https://gitlab.com/vatsalshah/foodtruckgenie.git`)
3. Install dependencies. (`$ pip install -r requirements.txt`)
4. Start Django server and celery worker. (`$ honcho start`)

## Technologies Used
- Base Stack: Python 3.5.0 w/ Django 1.9
- Tasks: Celery + Redis for scheduled and background tasks
- Database: Postgres
- Web App: HTML5 + CSS3 (Bootstrap) + JS (jquery)

## Shortcomings (Possible improvements that can be worked on with time)
1. Web Application:
    - Switch to AngularJS or ReactJS for scalability and maintainability.
    - Social and traditional authentication to complete the REST authentication, which is hard-coded for demo purpose
    - User preferences, favorites and history for quick access
    - Navigation
    - Cart Reviews
    - Results sorted by distance
    - Marker labels
    - Redesign UI/UX
    - UI testing on different screen sizes
2. Backend:
    - Switch to GeoDjango and PostGIS
    - More filters in the API
    - Add data sources
    - Modify database update task to maintain state and regularly run to keep it uptodate.
    - Test Cases
    
## Acknowledgement

- [Bootply Bootstrap Template](http://www.bootply.com/129821)

## Further Reading

- [Gunicorn](https://warehouse.python.org/project/gunicorn/)
- [WhiteNoise](https://warehouse.python.org/project/whitenoise/)
- [django-postgrespool](https://warehouse.python.org/project/django-postgrespool/)
- [dj-database-url](https://warehouse.python.org/project/dj-database-url/)
