from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from FoodTruckGenie import settings
from core.views.home import Home

urlpatterns = [
    url(r'^$', Home.as_view(), name='home'),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^core/', include('core.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
