
$(document).ready(function(){
    var map;
    var myMarker;
    var truckMarkers = [];
    var distanceSlider = $( "#slider" );
    var searchInput = $('#search-input');
    var auth_token;

    /* google maps -----------------------------------------------------*/
    google.maps.event.addDomListener(window, 'load', initialize);

    // Get user location
    function getUserLocation(callback) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(callback, callback(false));
        } else {
            console.log("Geolocation is not supported by this browser.");
            callback(false);
        }
    }

    // User login to get token for REST APIs
    function userLogin(username, password, callback) {
        var data = {
            username: username,
            password: password
        };
        $.ajax({
            url: encodeURI("/rest-auth/login/"),
            type: 'POST',
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (result) {
                callback(result, false);
            },
            error: function (err) {
                callback(false, err);
            }
        });
    }

    function initialize() {
        var latlng;
        userLogin("demo", "ftgenie", function(success, error){
            if (error) console.dir(error);
            else {
                auth_token = "Token " + success.key;
                getUserLocation(function(position){
                    if (position){
                        latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    } else {
                        /* position San Francisco */
                        latlng = new google.maps.LatLng(37.773972, -122.431297);
                    }
                    var mapOptions = {
                        center: latlng,
                        scrollWheel: false,
                        zoom: 15
                    };
                    myMarker = new google.maps.Marker({
                        position: latlng,
                        url: '/',
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                        icon: myMarkerIconURL
                    });

                    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                    myMarker.setMap(map);

                    google.maps.event.addListener(myMarker, 'dragend', function() {
                        map.setCenter(myMarker.position);
                        updateTruckMarkers(distanceSlider.slider( "value" ), searchInput.val())
                    });

                    // Distance Slider
                    distanceSlider.slider({
                        value:2,
                        min: 0,
                        max: 50,
                        step: 1,
                        slide: function( event, ui ) {
                            $( "#distance" ).val( ui.value + "miles" );
                            updateTruckMarkers(ui.value, searchInput.val())
                        }
                    });
                    $( "#distance" ).val( distanceSlider.slider( "value" ) + " miles" );
                    updateTruckMarkers(distanceSlider.slider( "value" ), searchInput.val())
                });
            }
        });
    }
    /* end google maps -----------------------------------------------------*/

    // Get user's current location
    $("#my-location").click(function(){
        var latlng;
        getUserLocation(function(position){
            if (position){
                latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            } else {
                /* position San Francisco */
                latlng = new google.maps.LatLng(37.773972, -122.431297);
            }
            map.setCenter(latlng);
            myMarker.setPosition(latlng);
            updateTruckMarkers(distanceSlider.slider( "value" ), searchInput.val())
        });
    });

    // Food item searches
    searchInput.on("keyup", function (e) {
        e.preventDefault();
        if (e.which == 13) {
            updateTruckMarkers(distanceSlider.slider( "value" ), this.value)
        }
    });

    $('#search-icon').on("click", function(){
        updateTruckMarkers(distanceSlider.slider( "value" ), searchInput.val())
    });

    // This will stop the submit of the form but allow the native HTML5 validation (which is what i believe you are after)
    $("form").on('submit',function(e){
        e.preventDefault();
    });

    function getCarts(latitude, longitude, distance, search_text, callback){
        var location = {
            latitude: latitude,
            longitude: longitude,
            distance: distance
        };
        var data = {
            location: JSON.stringify(location)
        };
        if (search_text){
            data['food_items'] = search_text;
        }
        $.ajax({
            url: encodeURI("/core/api/v1/truck/"),
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', auth_token);
            },
            data: data,
            contentType: 'application/json',
            success: function (result) {
                callback(result, false);
            },
            error: function (err) {
                callback(false, err);
            }
        })
    }

    function updateTruckMarkers(distance, search){
        var search_text;
        if (search && search !== ''){
            search_text = search;
        } else search_text = false;

        getCarts(myMarker.getPosition().lat(), myMarker.getPosition().lng(), distance, search_text, function(result, error){
            if (result){
                deleteTruckMarkers();
                $('#item_list').empty();
                for (var i = 0; i < result.length; i++) {
                    var latlng = new google.maps.LatLng(result[i]['latitude'], result[i]['longitude']);
                    addItemToList(result[i]);
                    addTruckMarker(latlng, result[i])
                }
            } else console.log(error);
        })
    }

    // Adds a marker to the map and push to the array.
    function addTruckMarker(location, truck) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            animation: google.maps.Animation.DROP,
            icon: truckIconURL,
            url: "#" + truck['objectid']
        });
        marker.info = new google.maps.InfoWindow({
            content: truckHTMLGenerator(truck)
        });
        google.maps.event.addListener(marker, 'click', function() {
            marker.info.close();
            scrollTo(this.url)
        });
        google.maps.event.addListener(marker, 'mouseover', function() {
            marker.info.open(map, marker);
        });
        google.maps.event.addListener(marker, 'mouseout', function() {
            marker.info.close();
        });
        truckMarkers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < truckMarkers.length; i++) {
            truckMarkers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearTruckMarkers() {
        setMapOnAll(null);
    }

    // Scrolls the page to a particular truck
    function scrollTo(hash) {
        location.hash = hash;
    }

    // Deletes all markers in the array by removing references to them.
    function deleteTruckMarkers() {
        clearTruckMarkers();
        truckMarkers = [];
    }

    // Add item to the list
    function addItemToList(truck){
        var truckHTML = truckHTMLGenerator(truck);
        $("#item_list").append(truckHTML)
    }

    // HTML Generator
    function truckHTMLGenerator(truck){
        var headerTitle = truck['applicant'];
        var anchorID = truck['objectid'];
        var address = truck['address'];
        var directions = truck['locationdescription'];
        var hours = truck['dayshours'];
        var foodItems = truck['fooditems'];
        var headerDiv = '<div class="panel panel-default"><div class="panel-heading"><a name=\"'+ anchorID +
            '\">' + headerTitle + " - " + address +'</a></div>';
        var content = '<p><b>Directions:</b> ' + directions + '<br><b>Hours of operation:</b> ' + hours +
            '<br><b>Food items:</b> ' + foodItems + '</p><hr>';
        return headerDiv + content
    }

});