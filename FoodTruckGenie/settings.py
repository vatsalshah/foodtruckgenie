"""
Django settings for FoodTruckGenie project. Setup for an environment pipeline DEV, STAGING and PROD.
"""
import json
import logging
import os
from urllib.request import Request, urlopen

import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


# Setup logging

logger = logging.getLogger('ftgenie-settings')

# Set application name

APPLICATION_NAME = "FoodTruckGenie"

# Get the application environment

APP_ENV = os.environ['FTGENIE_ENV']
logger.info("Application Environment: {}".format(APP_ENV))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "!dt(vn3l9f(vr6@6nn5-bc)974**1j&8k3ib4gh__m=*t2)pnc"

if APP_ENV == "PROD" or APP_ENV == "STAGING":
    SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
template_debug = True

if APP_ENV == "PROD":
    DEBUG = False
    template_debug = False

logger.info("DEBUG: {}".format(DEBUG))
logger.info("TEMPLATE_DEBUG: {}".format(template_debug))

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'djcelery_email',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'core'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'FoodTruckGenie.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': template_debug,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'django.contrib.auth.context_processors.auth',
            ],
        },
    },
]

WSGI_APPLICATION = 'FoodTruckGenie.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Parse database configuration from $DATABASE_URL

DATABASES = {'default': dj_database_url.config()}

# Setup Django db backend engine

DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

# Media Files (User Uploaded, Server Created on the Fly)

MEDIA_ROOT = 'mediafiles'
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

# AWS Configuration - S3 for media files
MEDIAFILES_LOCATION = 'media'
if APP_ENV == "PROD" or APP_ENV == "STAGING":
    AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
    AWS_STORAGE_BUCKET_NAME = os.environ['S3_BUCKET_NAME']

    S3_CUSTOM_DOMAIN = 's3-ap-southeast-1.amazonaws.com/%s' % AWS_STORAGE_BUCKET_NAME

    MEDIA_URL = "https://%s/%s/" % (S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
    DEFAULT_FILE_STORAGE = 'FoodTruckGenie.utils.custom_storages.MediaStorage'


# CORS Configuration
CORS_ORIGIN_ALLOW_ALL = True
if APP_ENV == "PROD":
    CORS_ORIGIN_ALLOW_ALL = False
    CORS_ORIGIN_WHITELIST = (

    )

# Celery Configuration

BROKER_URL = os.environ['REDIS_URL'],
CELERY_RESULT_BACKEND = os.environ['REDIS_URL']
CELERY_ACCEPT_CONTENT = ['pickle', 'json']
CELERY_TASK_SERIALIZER = 'pickle'

# Mail Setup
# To send all email via celery tasks
EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'

if APP_ENV == "PROD":
    logger.info("Email Handler: Mandrill")
    MANDRILL_API_KEY = os.environ['MANDRILL_API_KEY']
    CELERY_EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
    DEFAULT_FROM_EMAIL = "Team Sendd <support@sendd.co>"
elif APP_ENV == "STAGING":
    logger.info("Email Handler: Mailtrap")
    MAILTRAP_API_TOKEN = os.environ['MAILTRAP_API_TOKEN']
    request = Request("https://mailtrap.io/api/v1/inboxes.json?api_token={}".format(MAILTRAP_API_TOKEN))
    response_body = urlopen(request).read().decode('utf-8')
    credentials = json.loads(response_body)[0]

    EMAIL_HOST = credentials['domain']
    EMAIL_HOST_USER = credentials['username']
    EMAIL_HOST_PASSWORD = credentials['password']
    EMAIL_PORT = credentials['smtp_ports'][0]
    EMAIL_USE_TLS = True
else:
    logger.info("Email Handler: Console")
    CELERY_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Django REST Framework settings
REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
        'rest_framework.filters.SearchFilter'
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}

# Log settings

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'error_logfile': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'FoodTruckGenie/logs/errors.log',
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 7,
            'formatter': 'verbose',
        },
        'debug_logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'FoodTruckGenie/logs/debug.log',
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 7,
            'formatter': 'verbose',
            'filters': ['require_debug_true'],
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'debug_logfile', 'error_logfile', 'mail_admins'],
        },
        'ftgenie-settings': {
            'handlers': ['console', 'debug_logfile', 'error_logfile', 'mail_admins'],
        },
        'ftgenie-core': {
            'handlers': ['console', 'debug_logfile', 'error_logfile', 'mail_admins'],
        },
        'ftgenie-webapp': {
            'handlers': ['console', 'debug_logfile', 'error_logfile', 'mail_admins'],
        },
        'py.warnings': {
            'handlers': ['console', 'debug_logfile'],
        },
    }
}
